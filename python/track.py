#! /usr/bin/env python

import json
import sys
import math
import copy

class Track:
    MAX_LENGTH = 20
    MAX_ANGLE = 10

    def __init__(self, pieces):
        self.pieces = pieces
        # self.simpleTrack, self.idxMap = self.simplifyTrack(pieces)
        self.choppedTrack, self.idxMap = self.chopTrack(pieces)
        # for i in range(len(pieces)):
        #     print pieces[i], self.idxMap[i],self.simpleTrack[self.idxMap[i]]


    # This function will either accept a json piece or will be an index to
    # UNSIMPLIFIED TRACK PIECE
    def isStraight(self,piece):
        try:
            if 'length' in piece.keys():
                return True
        except:
            if 'length' in self.pieces[piece].keys():
                return True
        return False

    # This function will either accept a json piece or will be an index to
    # UNSIMPLIFIED TRACK PIECE
    def isCurved(self,piece):
        try:
            if 'radius' in piece.keys():
                return True
        except:
            if 'radius' in self.pieces[piece].keys():
                return True
        return False

    # This function will either accept a json piece or will be an index to
    # UNSIMPLIFIED TRACK PIECE
    def isSwitch(self,piece):
        try:
            return piece['switch']
        except:
            pass
        try:
            return self.pieces[piece]['switch']
        except:
            return False

    def getPiece(self, idx):
        return self.pieces[idx]

    # This functions will take a defined track and will combine:
    #   - Straight sections no matter the length
    #   - Curved sections if they have the same radius of curvature and 
    #     turn direction
    #   It will return a simplified track and a map of pieceIndices to simpleTrackPiecesIndices
    def simplifyTrack(self, pieces):
        simpleTrack = [copy.deepcopy(pieces[0])]
        idxMap = [0]
        for piece in pieces[1:]:
            simplePiece = simpleTrack[-1]
            processed = False

            # If both pieces are straight, then combine their lengths
            if self.isStraight(simplePiece) and self.isStraight(piece):
                simplePiece['length'] += piece['length']
                processed = True
            
            # If both piece are curved and their signs match and the radius is the same
            if self.isCurved(piece) and self.isCurved(simplePiece):
                if math.copysign(1,piece['angle']) == \
                        math.copysign(1,simplePiece['angle']) and \
                        piece['radius'] == simplePiece['radius']:
                    simplePiece['angle'] += piece['angle']
                    processed = True

            # Otherwise we create a new piece in simpleTrack
            if not processed:
                simpleTrack.append(copy.deepcopy(piece))

            idxMap.append(len(simpleTrack)-1)
        return simpleTrack,idxMap        
        

    def chopPiece(self,piece):
        choppedPiece = []
        print piece
        if 'length' in piece.keys():
            while piece['length'] > 0:
                if piece['length'] > self.MAX_LENGTH:
                    choppedPiece.append({'length':self.MAX_LENGTH})
                    piece['length'] -= self.MAX_LENGTH
                else:
                    choppedPiece.append({'length':piece['length']})
                    piece['length'] = 0
        elif 'angle' in piece.keys():
            while abs(piece['angle']) > 0:
                if abs(piece['angle']) > self.MAX_ANGLE:
                    angle = piece['angle']
                    choppedPiece.append(
                        {'angle':math.copysign(self.MAX_ANGLE,angle),
                        'radius':piece['radius']})
                    piece['angle'] -= math.copysign(self.MAX_ANGLE,angle)
                else:
                    choppedPiece.append(
                        {'angle':piece['angle'],
                        'radius':piece['radius']})
                    piece['angle'] = 0

        print choppedPiece
        return choppedPiece

    def chopTrack(self,pieces):
        idxMap = []
        choppedTrack = []

        for i,piece in enumerate(pieces):
            p = copy.deepcopy(piece)
            choppedPiece = self.chopPiece(piece)
            choppedTrack += choppedPiece
            idxMap += [i] * len(choppedPiece)


        return choppedTrack, idxMap

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print 'Incorrect Usage: python track.py <filename>'
    with open(sys.argv[1]) as f:
        Track(json.load(f)['race']['track']['pieces'])
