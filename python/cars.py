# cars.py
#
# Helper class for getting cars info

class Cars(object):

    def __init__(self, myCarName):
    	self.myCarName = myCarName

   	def getMyCar(self, carPositions):
   		for car in carPositions:
   			if car['id']['name'] == self.myCarName:
   				return car

