# FortuneTeller.py
#
# Takes in info about future danger on track and computes the throttle necessary.

class FortuneTeller(object):
    """FortuneTeller"""

    BASE_LOOK_AHEAD = 100
    LOOK_AHEAD_DELTA = 10
    BIN_WIDTH = 5     # look at this # of track sections 
    WEIGHTS = [0.1,0.2,0.5,0.1,0.1] # same length as BIN_WIDTH

    def __init__(self, choppedTrack):
        self.choppedTrack = choppedTrack


    def computeThrottle(self, carPiecePosition):
        # Step 1: find our current position
        currentSegment = self.choppedTrack.getSegment(carPiecePosition['piecePosition'], carPiecePosition['inPieceDistance']) 

        # Step 2: find track dangers
        danger = 0
        for i in xrange(BIN_WIDTH):
            # TODO: check if this is how to use the track danger
            danger_idx = self.choppedTrack.getDanger(currentPosition + (i-BIN_WIDTH/2)*LOOK_AHEAD_DELTA)
            danger += WEIGHTS[i]*self.trackDanger.getDanger(danger_idx)

        # Step 3: compute throttle
        throttle = 1 - danger

        return throttle

